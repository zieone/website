+++
date = "2018-08-12"
title = "Storing passwords for services"
+++

== Storing user passwords

Use scrypt if possible for storing users passwords in your authentication system, otherwise PBKDF2 or bcrypt.

Sources:

"scrypt is asymptotically much more expensive to crack." -cpercival via (https://news.ycombinator.com/item?id=9593916)

"I skip bcrypt because the only reason to not use scrypt is if you need a US Government endorsed scheme. But yeah, it's (slightly) better than PBKDF2." -cpercival via https://news.ycombinator.com/item?id=16748400

Then bcrypt or PBKDF2
"bcrypt is asymptotically marginally more expensive to crack than PBKDF2, but not enough to matter; I'm guessing tptacek's point here is that bcrypt has more library support available (despite PBKDF2 being the de jure standard). I wouldn't say there's a strong argument in either direction." -cpercival via https://news.ycombinator.com/item?id=9593916

"scrypt has nice properties that bcrypt doesn't, and gets those properties by design; it turns out that in practice right now bcrypt has some nice properties too, though they seem accidental. We're using scrypt at Starfighter, even though we have to go through a (very minor) bit of trouble to get it. They're all fine though." -tptacek via (https://news.ycombinator.com/item?id=9593916) May 23, 2015
