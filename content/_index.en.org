#+TITLE: Welcome
#+DATE: <2018-09-12 Wed>

We are a few people dedicated to increasing knowledge.  zie.one is named after zie, which is a 3rd gender term, not identified as either Male or Female.  If this sounds like you, feel free to get in touch!

If you are interested in our Secure Scuttlebutt(SSB) open pub, try [[file:/ssb/][here]].
If you are interested in computer security best practices, try [[file:/security/][here]].

Our language translations are currently miserable, [[https://gitlab.com/zieone/website][contributions welcome]]!

This website is proud to not use any javascript. Nor do we do any tracking or analytics, you are welcome!

 All content is licensed [[file:CCBY4.png][http://creativecommons.org/licenses/by/4.0/]][[http://creativecommons.org/licenses/by/4.0/][CC By 4]].